package br.ucsal.bes20192.testequalidade.restaurante.enums;

public enum SituacaoMesaEnum {
	LIVRE, OCUPADA;
}
