package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {
	private static final SituacaoMesaEnum SITUACAO_PADRAO = SituacaoMesaEnum.LIVRE;
	private static final Integer NUMERO_PADRAO = 1;
	private static final Integer CAPACIDADE_PADRAO = 3;
	
	private SituacaoMesaEnum situacao = SITUACAO_PADRAO;
	private Integer numero = NUMERO_PADRAO;
	private Integer capacidade = CAPACIDADE_PADRAO;
	
	private MesaBuilder() {
	}
	
	public static MesaBuilder umaMesa() {
		return new MesaBuilder();
	}	
	
	public MesaBuilder livre() {
		this.situacao = SituacaoMesaEnum.LIVRE;
		return this;
	}
	
	public Mesa build() {
		Mesa mesa = new Mesa(numero);
		mesa.setCapacidade(capacidade);
		mesa.setSituacao(situacao);
		return mesa;
	}
}
