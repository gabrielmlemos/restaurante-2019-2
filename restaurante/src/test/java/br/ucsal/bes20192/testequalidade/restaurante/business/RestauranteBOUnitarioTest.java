package br.ucsal.bes20192.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.bes20192.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20192.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20192.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20192.testequalidade.restaurante.persistence.MesaDao;

@RunWith(MockitoJUnitRunner.class)
public class RestauranteBOUnitarioTest {

	/**
	 * Método a ser testado: public void abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Crie um builder para instanciar a classe Mesa.
	 */
	
	@Mock
	private MesaDao mesaDAOMock;
	
	@Mock
	private ComandaDao comandaDAOMock;
	
	@InjectMocks
	private RestauranteBO restauranteBO;
	
	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException {
		// Entrada Dados
		Integer numeroMesa = 1;
		MesaBuilder mesaBuilder = MesaBuilder.umaMesa().livre();
		Mesa mesa = mesaBuilder.build();
		
		// Configurar Mock
		Mockito.when(mesaDAOMock.obterPorNumero(numeroMesa)).thenReturn(mesa);
		
		restauranteBO.abrirComanda(numeroMesa);
		
		Mockito.verify(comandaDAOMock).incluir((Comanda) Mockito.any(Object.class));
	}
}
